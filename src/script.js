$(function() {

  $('.js-popup-brochure__trigger').on('click', function(e) {
    e.preventDefault();
    $('html').addClass('is-locked');
    $('.js-popup-brochure').addClass('is-active');
    });

  $('.js-popup-brochure__send').on('click', function(e) {
    e.preventDefault();
    $('.js-popup-brochure').removeClass('is-active');
    $('.js-popup-thanks').addClass('is-active');
    });

  $('.js-popup-consultation__trigger').on('click', function(e) {
    e.preventDefault();
    $('html').addClass('is-locked');
    $('.js-popup-consultation').addClass('is-active');
    });

  $('.js-popup-consultation__send').on('click', function(e) {
    e.preventDefault();
    $('.js-popup-consultation').removeClass('is-active');
    $('html').removeClass('is-locked');
    });

  $('.js-close').on('click', function(e) {
    e.preventDefault();
    $('html').removeClass('is-locked');
    $('.js-popup').removeClass('is-active');
    });

  });